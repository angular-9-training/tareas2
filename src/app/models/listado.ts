import { Tarea } from './tarea';

export class Listado {
  tareas: Tarea[] = [];

  constructor() { }
  
  añadeTarea(tarea: Tarea) {
    this.tareas.push(tarea as Tarea);
  }

  borrar(tarea: Tarea): boolean {
    let index: number;

    // Obtenemos posición de la tarea en el listado
    index = this.tareas.indexOf(tarea);
    // Si lo ha encontrado
    if (index >= 0) {
      this.tareas.splice(index, 1);
      return true;
    } else {
      return false;
    }
  }

}
