import { Importancia } from './importancia.enum';

export class Tarea {

  nombre: string;
  importancia: Importancia;
  fecha_finalizacion: Date;

  constructor(nombre: string, importancia: Importancia, fecha_finalizacion: Date) {
    this.nombre = nombre;
    this.importancia = importancia;
    this.fecha_finalizacion = fecha_finalizacion;
  }

  formateaFecha(): string {
    return this.fecha_finalizacion.toISOString().substr(1, 11);
  }


}
