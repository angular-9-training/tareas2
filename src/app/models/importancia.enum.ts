export enum Importancia {
  MUY_BAJA,
  BAJA,
  MEDIA,
  MEDIA_ALTA,
  ALTA
}
