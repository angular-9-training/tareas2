import { Component, OnInit } from '@angular/core';
import { Tarea } from 'src/app/models/tarea';
import { Listado } from 'src/app/models/listado';
import { Importancia } from 'src/app/models/importancia.enum';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.sass']
})
export class ListaTareasComponent implements OnInit {

  listadoActivas: Listado;
  listadoCompletas: Listado;
  //tareaNueva: Tarea = new Tarea('Introduce nombre tarea', Importancia.MUY_BAJA, new Date());
  nombreTareaNueva: string; 
  importanciaTareaNueva: Importancia;
  fechaFinalizacionTareaNueva: Date; 

  constructor() {
    this.listadoActivas = new Listado();
    this.listadoCompletas = new Listado();
  }

  ngOnInit(): void {
  }

  crearTarea() {
    const nuevaTarea: Tarea = new Tarea(this.nombreTareaNueva,  this.importanciaTareaNueva, this.fechaFinalizacionTareaNueva );
    this.listadoActivas.añadeTarea(nuevaTarea);
  }

  completarTarea(event: any) {
    const tareaACompletar: Tarea = event as Tarea;
   
    this.listadoActivas.borrar(tareaACompletar);
    this.listadoCompletas.añadeTarea(tareaACompletar);
    
  }

  borrarTarea(event: any) {
    const tareaABorrar: Tarea = event as Tarea; 
    this.listadoCompletas.borrar(tareaABorrar);

  }

}
