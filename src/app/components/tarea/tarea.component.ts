import { Component, OnInit, Input, Output , EventEmitter} from '@angular/core';
import { Tarea } from 'src/app/models/tarea';
import { Importancia } from 'src/app/models/importancia.enum';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.sass']
})
export class TareaComponent implements OnInit {

  @Input() tarea: Tarea;

  @Output() tareaACompletar = new EventEmitter<Tarea>();

  constructor() { }

  ngOnInit(): void {
  }

  completarTarea() {
    const tareaCompleta = this.tarea;
    this.tareaACompletar.emit(tareaCompleta);
  }

  dameEstiloTarea(): string {
    let estilo: string;
    switch (this.tarea.importancia) {
      case Importancia.MUY_BAJA:
        estilo = 'panel panel-success';
        break;
      case Importancia.BAJA:
        estilo = 'panel panel-primary';
        break;
      case Importancia.MEDIA:
        estilo = 'panel panel-secondary';
        break;
      case Importancia.MEDIA_ALTA:
        estilo = 'panel panel-warning';
        break;
      case Importancia.ALTA:
        estilo = 'panel panel-danger';
        break;
      default:
        estilo = "";
    }
    return estilo;
  }

}
